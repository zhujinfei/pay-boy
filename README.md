# 项目描述
payboy（贝帛吱付）的目标是打造一款开源的、轻量级的、微服务化的、可公私有云部署的、可定制化的集成聚合支付和资金清结算于一体的四方支付平台，满足互联网企业业务系统的收款和资金处理等需求。

<hr>

# 说明文档 

查看文档可以快速上手, [传送门](https://gitee.com/ndood/pay-boy/wikis)

<hr>

# 项目计划--一期（详见qq群安排）

| 序号 | 计划阶段 | 目标 | 项目组 | 状态 | 
| --: | :--: | :--: | :--: | :-- |
| 1 | 需求获取 | 领域模型，用例模型 | 需求组 | 进行中 |
| 2 | 分析 | 对象模型，动态模型 | 需求组 | 进行中 |
| 3 | 系统设计 | 软件原型，架构，架构演进 | 需求组，架构组 | 进行中 |
| 4 | 详细设计 | 接口文档，数据模型 | 研发组 | 进行中 |
| 5 | 编码 | 编码，代码重构 | 研发组 | 未开始 |
| 6 | 测试 | 测试，性能调优，安全 | 研发组，运维组 | 未开始 |
| 7 | 运维 | 部署，监控，升级维护 | 运维组 | 未开始 |

<hr>

# 参与开发（贡献时间顺序）

## 需求组
酷ma萌宝宝，ndood

## 架构组
owen，zscat，yangyz，wallacefw，ndood

## 研发组
victhon，zhouxiaoxiong，lyg945，hotant，路过某天，Aron，血色孤狼，Kevin_Zhan，Package，半醉半醒半浮生，wenwen，zhanghc321，居慕凝，ndood

## 运维组
yangyz，ndood

<hr>

# 业务结构

## 支付接口

支付 退款 查询 撤销 异步通知 充值 充值查询 充值撤销 

## 运营平台

配置 权限 支付流水 清算 结算 对账 商户 风控 渠道路由

## 商户平台

配置 权限 支付流水 清算 结算 对账 

## 商城后台

配置 权限 商品 库存 订单 充值订单 统计 

## 商城前台

商品展示 充值商品 购物车 我的账户 我的订单 

## 收银台

被扫 扫码 门店码 订单查询 退款 收银统计 

<hr>

# 技术选型

## 后端技术

springboot2.0，
springdata，
spring-security，
spring-oauth，
spring-social，
spring-cloud，
jooq，
mybatis，
websocket，
redis，
mysql，
rabbitmq， 
docker

## 前端技术

bootstrap，
thymeleaf，
requirejs，
layui，
vuejs

<hr>

# 效果展示

![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233051_5c45f7d0_754149.png "screen1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233106_a4311d10_754149.png "screen2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233116_87eb038e_754149.png "screen3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233126_98368b9f_754149.png "screen4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233135_55904a1d_754149.png "screen5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233151_0d244527_754149.png "screen6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233204_19f755ea_754149.png "screen7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233214_df8aba5b_754149.png "screen8.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233224_bbc5c58c_754149.png "screen9.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/233234_5ef59f3c_754149.png "screen10.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1109/111338_20005491_754149.png "screen11.png")

<hr>

# 交流、反馈、参与贡献

* QQ群：460395575
* E-mail：908304389@qq.com
* gitee地址：https://gitee.com/ndood/pay-boy

<hr>

# 参考资料

* https://www.cnblogs.com/lucky_hu/p/9823406.html?from=timeline
* https://wk.baidu.com/view/65b7786b376baf1ffc4fadca.html?fromShare=1&from=timeline
* https://www.jianshu.com/p/ea1e4875166a
* http://www.498.net/cpzx/article-510.html
* http://kf.qq.com/product/wechatpaymentmerchant.html
* https://cshall.alipay.com/lab/question.htm
* http://doc.cocolian.cn