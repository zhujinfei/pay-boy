package com.ndood.authenticate.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Unsatisfied dependency expressed through field 'authenticationManager'; 
 * nested exception is org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 
 * 'org.springframework.security.authentication.AuthenticationManager' 
 * available: expected at least 1 bean which qualifies as autowire candidate. 
 * Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
 */
@Configuration
public class AppSecurityBeanConfig extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        AuthenticationManager manager = super.authenticationManagerBean();
        return manager;
    }

}