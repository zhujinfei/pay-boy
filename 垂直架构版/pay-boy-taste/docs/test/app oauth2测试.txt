oauth2测试 6.2

1 *授权码模式-获取授权码
GET http://localhost:8080/oauth/authorize?response_type=code&client_id=f61b7ca2-3dd5-4835-b95d-e37f824c9642&redirect_uri=https://www.baidu.com&scope=all
	f61b7ca2-3dd5-4835-b95d-e37f824c9642  g6237ca2-3dd5-4835-b95d-e37f824c9643
	配置security oauth2后
		taiyi  taiyi

2 授权码模式-获取授权码
GET http://localhost:8080/oauth/authorize?response_type=code&client_id=taiyi&redirect_uri=http://example.com&scope=all
	自定义userDetailsService，把oauth覆盖	
		taiyi  123456

3 授权码模式-获取token
POST http://localhost:8080/oauth/token
	HEADERS 
		Content-Type: application/x-www-form-urlencoded  
		Authorization: Basic dGFpeWk6dGFpeWk=
	BODY
		grant_type: authorization_code
		client_id: taiyi
		code: YxquV8
		redirect_uri: http://example.com
		scope: all

4 密码模式-获取token
POST http://localhost:8080/oauth/token
	HEADERS 
		Authorization: Basic dGFpeWk6dGFpeWk=
	BODY
		grant_type: password
		username: taiyi
		password: 123456
		scope: all
			
5 使用token访问资源
GET http://localhost:8080/user/me
	HEADERS
		Authorization: bearer eeb6bfa2-da8f-4344-a467-42d6c67b8976

--------------------------------------------------------------------------
oauth2测试 6.4

1. 自定义表单模式-获取token
POST http://127.0.0.1:8080/authentication/form
	HEADERS 
		Content-Type: application/x-www-form-urlencoded
		Authorization: Basic dGFpeWk6dGFpeWk=
	BODY
		username: ndood
		password: 123456

2. 自定义短信模式-获取短信验证码
GET http://127.0.0.1:8080/code/sms?mobile=13586003049
	HEADERS
		userId: 123

3. 自定义短信模式-获取token
POST http://127.0.0.1:8080/authentication/mobile
	HEADERS
		Authorization: Basic dGFpeWk6dGFpeWk=
		Content-Type: application/x-www-form-urlencoded
		userId: 123
	BODY
		mobile: 13586003049
		smsCode: 045378

--------------------------------------------------------------------------
oauth2测试 6.6

1. 微信简化模式
从数据库中获取openid
POST http://18p88z8050.imwork.net/authentication/openid
	HEADERS:
		Authorization: Basic dGFpeWk6dGFpeWk=
		Content-Type: application/x-www-form-urlencoded
	BODY:
		providerId: weixin
		openId: oiNe-0w8fC5hDy1pz4Cm-8LPX4qQ

2. 微信授权码模式
将demo的dependency改成cleansystem-authenticate-browser
Oauth2AuthenticationService 98打断点
http://18p88z8050.imwork.net/admin-signIn.html微信登录，进断点后停止
复制浏览器刚才的跳转连接中的code
POST http://18p88z8050.imwork.net/qqLogin/weixin?code=YxquV8&state=a2104078-bfaf-4c97-a75b-acb57cf4c56d
	HEADERS:
		Authorization: Basic dGFpeWk6dGFpeWk=

--------------------------------------------------------------------------
oauth2测试6.7

1. 微信授权码模式
将数据库auth_connection中的记录删除
将AdminConnectionSignUp的Component注解移除
将demo的dependency改成cleansystem-authenticate-browser
将UserController中的signUpUtils切换成浏览器的
Oauth2AuthenticationService 98打断点
http://18p88z8050.imwork.net/admin-signIn.html微信登录，进断点后停止
复制浏览器刚才的跳转连接中的code
POST http://18p88z8050.imwork.net/qqLogin/weixin?code=YxquV8&state=a2104078-bfaf-4c97-a75b-acb57cf4c56d
	HEADERS:
		Authorization: Basic dGFpeWk6dGFpeWk=
		userId: 123

2. 微信授权码模式跳转
GET http://18p88z8050.imwork.net/social/user
	HEADERS:
		userId: 123

3. 微信授权码模式注册
POST http://18p88z8050.imwork.net/user/regist
	HEADERS:
		userId: 123

4. 微信授权码模式静默注册
将数据库auth_connection中的记录删除
将AdminConnectionSignUp的Component注解加上
将demo的dependency改成cleansystem-authenticate-browser
将UserController中的signUpUtils切换成浏览器的
Oauth2AuthenticationService 98打断点
http://18p88z8050.imwork.net/admin-signIn.html微信登录，进断点后停止
复制浏览器刚才的跳转连接中的code
POST http://18p88z8050.imwork.net/qqLogin/weixin?code=YxquV8&state=a2104078-bfaf-4c97-a75b-acb57cf4c56d
	HEADERS:
		Authorization: Basic dGFpeWk6dGFpeWk=
		userId: 123
		
--------------------------------------------------------------------------
oauth2测试6.8

1. oauth2密码方式认证
POST http://18p88z8050.imwork.net/oauth/token
	HEADERS: 
		Authorization: Basic dGFpeWk6MTIzNDU2
		Content-Type : application/x-www-form-urlencoded
	BODY:
		grant_type[Text]=password
		username[Text]=taiyi
		password[Text]=123456
		scope[Text]=all

2. 使用令牌访问服务
GET http://18p88z8050.imwork.net/user/me
	HEADERS: 
		Authorization : bearer eeb6bfa2-da8f-4344-a467-42d6c67b8976

--------------------------------------------------------------------------
oauth2测试6.9

1. oauth2密码方式认证
POST http://18p88z8050.imwork.net/oauth/token
	HEADERS: 
		Authorization: Basic dGFpeWk6MTIzNDU2
		Content-Type: application/x-www-form-urlencoded
	BODY:
		grant_type[Text]=password
		username[Text]=taiyi
		password[Text]=123456
		scope[Text]=all
		
2. 使用令牌访问服务
GET http://18p88z8050.imwork.net/user/me
	HEADERS: 
		Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjQ1Mzg2ODIsInVzZXJfbmFtZSI6InRhaXlpIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfVVNFUiJdLCJqdGkiOiJhZDE3ZDQ0OS05ZjkzLTQ2NDAtOGViOC0yMzZiNmRhYTgwZjgiLCJjbGllbnRfaWQiOiJ0YWl5aSIsInNjb3BlIjpbImFsbCJdfQ.EyCAExYKySBB7mt9D8XGX5eI_xBtPTL8TjnhN3alGpk
		
3. 刷新token
POST http://18p88z8050.imwork.net/oauth/token
	HEADERS: 
		Authorization: Basic dGFpeWk6MTIzNDU2
		Content-Type: application/x-www-form-urlencoded
	BODY:
		grant_type : refresh_token
		refresh_token : yyyyyyyyyyyyyyyy
		scope : all
		
--------------------------------------------------------------------------
oauth2测试7.4

1. 浏览器环境下，自定义密码验证码模式登录
GET http://18p88z8050.imwork.net/admin-signIn.html

2. 自定义了授权模块后，访问提示没有权限
GET http://18p88z8050.imwork.net/user
