package com.ndood.merchant.pojo.comm.vo;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.ndood.merchant.core.constaints.MerchantCode;

/**
 * 普通对象返回数据
 */
public class MerchantResultVo implements Serializable {
	private static final long serialVersionUID = 3765308055181715685L;

	public final static MerchantResultVo ok() {
		return new MerchantResultVo(MerchantCode.SUCCESS);
	}
	
	public final static MerchantResultVo error() {
		return new MerchantResultVo(MerchantCode.ERR_OTHER);
	}
	
	private MerchantCode code;
	private String msg;
	private Object data;
	
	private MerchantResultVo(MerchantCode code) {
		this.code = code;
		this.msg = code.getValue();
	}

	@JSONField(name = "code")
	public String getCode() {
		return code.getCode();
	}
	
	@JSONField(name = "code")
	public MerchantResultVo setCode(String code) {
		this.code = MerchantCode.getEnum(code);
		return this;
	}

	public String getMsg() {
		return msg;
	}
	
	public MerchantResultVo setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public Object getData() {
		return data;
	}

	public MerchantResultVo setData(Object data) {
		this.data = data;
		return this;
	}
}
