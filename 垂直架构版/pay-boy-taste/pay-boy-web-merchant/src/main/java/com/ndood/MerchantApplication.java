package com.ndood;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Merchant启动类
 * https://github.com/yelingfeng/vuex-tutorial
 */
@SpringBootApplication
@EnableWebMvc
@EnableSwagger2
@EnableCaching
@EnableTransactionManagement
public class MerchantApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(MerchantApplication.class, args);
	}
	
}