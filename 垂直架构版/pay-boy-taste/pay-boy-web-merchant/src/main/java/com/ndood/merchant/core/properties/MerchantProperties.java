package com.ndood.merchant.core.properties;
/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * ndood admin配置主类
 * @author ndood
 */
@Data
@ConfigurationProperties(prefix = "ndood.merchant")
public class MerchantProperties {
	
	private String domain = "http://pay.jeepupil.top";
	
	private Boolean isDevelop = true;
	
	/**
	 * 阿里云通信相关配置
	 */
	private AliyunSmsProperties dayu = new AliyunSmsProperties();
	/**
	 * 阿里云OSS相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
	/**
	 * 静态资源相关配置
	 */
	private StaticResourceProperties stat = new StaticResourceProperties();
	/**
	 * 邮件相关配置
	 */
	private MerchantEmailProperties email = new MerchantEmailProperties();
}