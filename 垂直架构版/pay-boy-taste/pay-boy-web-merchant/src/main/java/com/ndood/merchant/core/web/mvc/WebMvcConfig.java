package com.ndood.merchant.core.web.mvc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.ndood.merchant.core.properties.MerchantProperties;

/**
 * 添加WebConfig类 解决找不到静态资源的问题
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private MerchantProperties merchantProperties;
	
	/**
	 * 静态资源映射
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		
		if(merchantProperties.getIsDevelop()) {
			String location = "classpath:/static/native/";
			registry.addResourceHandler("/static/**").addResourceLocations(location);
		}else {
			String location = "classpath:/static/build/";
			registry.addResourceHandler("/static/**").addResourceLocations(location);
		}
	}
	
	/**
	 * json转换
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		/*
		 * 1、需要先定义一个 convert 转换消息对象； 2、添加 fastJson 的配置信息，比如: 是否要格式化返回的Json数据； 3、在
		 * Convert 中添加配置信息; 4、将 convert 添加到 converts 中;
		 */

		// 1、需要先定义一个 convert 转换消息对象；
		FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();

		// 2、添加 fastJson 的配置信息，比如: 是否要格式化返回的Json数据；
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat, SerializerFeature.WriteDateUseDateFormat);
		fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");

		// 3、处理中文乱码问题
		List<MediaType> fastMediaTypes = new ArrayList<>();
		fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
		fastConverter.setSupportedMediaTypes(fastMediaTypes);

		// 4、在 Convert 中添加配置信息;
		fastConverter.setFastJsonConfig(fastJsonConfig);

		// 5、将 convert 添加到 converts 中;
		converters.add(fastConverter);
	}

	/**
	 * 解决springboot2.0不支持delete form的问题
	 * https://blog.csdn.net/moshowgame/article/details/80173817?utm_source=blogxgwz1
	 */
	@Bean
    public FilterRegistrationBean<HiddenHttpMethodFilter> testFilterRegistration3() {
        FilterRegistrationBean<HiddenHttpMethodFilter> registration = new FilterRegistrationBean<HiddenHttpMethodFilter>();
        registration.setFilter(new HiddenHttpMethodFilter());//添加过滤器
        registration.addUrlPatterns("/*");//设置过滤路径，/*所有路径
        registration.setName("HiddenHttpMethodFilter");//设置优先级
        registration.setOrder(2);//设置优先级
        return registration;
    }
	
}