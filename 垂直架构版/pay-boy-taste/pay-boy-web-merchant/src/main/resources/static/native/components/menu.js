// https://github.com/edgardleal/require-vuejs
define([
	'vue',
	'jquery',
	'jquery-extension',
],function(Vue){
	Vue.component('menuItem', Vue.extend({
	    name: 'menu-item',
	    props: {
	    	menuItems:[]
	    },
	    data: function(){
	    	return {
	    		menuItemList: this.menuItems
	    	}
	    },
	    computed: {
	    	// 计算属性
	    },
	    methods: {
	    	sendMsgToParent: function(event){
	    		this.$emit('listen-to-child-event',event)
	    	}
	    },
	    template: '<ul class="nav navbar-nav">'+
			'	<li class="menu-dropdown classic-menu-dropdown" v-bind:class="{ active: index==0 }" v-for="(menuItem,index) in menuItemList">'+
	        '		<a href="javascript:;"> {{menuItem.name}} <span class="arrow"></span></a>'+
	        '		<ul class="dropdown-menu pull-left" v-if="menuItem.subList.length">'+
	        '			<li v-for="sub in menuItem.subList" v-on:click="sendMsgToParent(sub.name)">'+
	        '				<a href="javascript:void(0)" class="nav-link">{{sub.name}}</a>'+
	        '			</li>'+
	        '		</ul>'+
	        '	</li>'+
	        '</ul>'
	}));
})
