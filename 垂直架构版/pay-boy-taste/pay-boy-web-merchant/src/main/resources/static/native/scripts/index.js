define(function(require){
	
	var Vue = require("vue");
//	var store = require("vuex/store");
	var menu =  require("components/menu");
	var $ = require("jquery");
	require("bootstrap");
	require("cookie");
	require("app");
	require("layout");
	require("quick-nav");
	new Vue({
	    el:'#page_index',
	    components:{
	    	menu: menu
	    },
//	    store: store,
	    ready: function(){
            $.material.init();
        },
	    created: function(){
	    },
	    updated: function(){
	    },
	    methods: {
	    	showMsgFromChild:function(msg){
	    		alert(msg);
	    	}
	    },
	    data:{
	    	menuItemList:[
	    		{
	    			name: '主页',
	    			type: '0',
	    			url: '#',
	    			subList:[]
	    		},
	    		{
	    			name: '交易中心',
	    			type: '0',
	    			url: '#',
	    			subList:[
	    				{
	    					name: '资金账户',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '交易订单',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '对账管理',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '结算管理',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '二维码收款',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '用户列表',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '交易统计',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    			]
	    		},
	    		{
	    			name: '账户中心',
	    			type: '0',
	    			url: '#',
	    			subList:[
	    				{
	    					name: '商户资料',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '秘钥设置',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '支付渠道设置',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '基本信息',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '账号绑定',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '修改密码',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    			]
	    		},
	    		{
	    			name: '系统管理',
	    			type: '0',
	    			url: '#',
	    			subList:[
	    				{
	    					name: '角色管理',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '权限管理',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '门店管理',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '店员管理',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '在线店员',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '操作日志',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    			]
	    		},
	    		{
	    			name: '消息通知',
	    			type: '0',
	    			url: '#',
	    			subList:[
	    				{
	    					name: '平台维护公告',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    				{
	    					name: '商户通知',
	    	    			type: '1',
	    	    			url: '#',
	    				},
	    			]
	    		}
	    	]
	    }
	});
});