package com.ndood.web.merchant.dao;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;

import org.jooq.DSLContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.ndood.merchant.jooq.Tables;
import com.ndood.merchant.jooq.tables.pojos.TJob;
import com.ndood.merchant.pojo.test.TJobDto;

/**
 * demo地址
 * https://blog.jooq.org
 * https://www.programcreek.com/java-api-examples/?api=org.jooq.SQLDialect
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDao {
	
    @Autowired
    private DSLContext dsl;
    
    com.ndood.merchant.jooq.tables.TJob jb = Tables.T_JOB;
    
    /** 
     * https://blog.jooq.org
     * 删除
     */
    @Test
    public void delete() {
        int execute = dsl.delete(jb).where(jb.ID.eq(2)).execute();
        System.out.println(execute);
    }

    /**
     * 增加
     */
    @Test
    public void insert() {
    	TJob job = new TJob();
    	job.setBeanClass("1");
    	job.setCreateBy("2");
    	job.setCreateTime(LocalDateTime.now());
    	job.setCronExpression("3");
    	job.setDescription("4");
    	job.setIsConcurrent("5");
    	job.setJobGroup("6");
    	job.setJobName("7");
    	job.setJobStatus("8");
    	job.setMethodName("9");
    	job.setSpringBean("10");
    	job.setUpdateBy("11");
    	job.setUpdateTime(LocalDateTime.now());
    	dsl.insertInto(jb).
            columns(jb.ID,jb.BEAN_CLASS,jb.CREATE_BY,jb.CREATE_TIME,jb.CRON_EXPRESSION,
            		jb.DESCRIPTION,jb.IS_CONCURRENT,jb.JOB_GROUP,jb.JOB_NAME,
            		jb.JOB_STATUS,jb.METHOD_NAME,jb.SPRING_BEAN,jb.UPDATE_BY,
            		jb.UPDATE_TIME).
            values(33,job.getBeanClass(),job.getCreateBy(),job.getCreateTime(),job.getCronExpression(),
            		job.getDescription(),job.getIsConcurrent(),job.getJobGroup(),job.getJobName(),
            		job.getJobStatus(),job.getMethodName(),job.getSpringBean(),job.getUpdateBy(),
            		job.getUpdateTime())
            .execute();
    }

    /**
     * 更新
     */
    @Test
    public void update() {
        int execute = dsl.update(jb)
	        .set(jb.METHOD_NAME,"asdasd")
	        .where(jb.ID.eq(Integer.valueOf(2)))
	        .execute();
        System.out.println(execute);
    }

    /**
     * 查询单个
     */
    @Test
    public void selectById() {
        /*TJob result =  dsl.select(jb.BEAN_CLASS,jb.CREATE_BY,jb.CREATE_TIME,jb.CRON_EXPRESSION,
        		jb.DESCRIPTION,jb.IS_CONCURRENT,jb.JOB_GROUP,jb.JOB_NAME,
        		jb.JOB_STATUS,jb.METHOD_NAME,jb.SPRING_BEAN,jb.UPDATE_BY,
        		jb.UPDATE_TIME)
                .from(jb)
                .where(jb.ID.eq(1)).fetchOneInto(TJob.class);*/
        
    	TJob result = dsl.select(jb.fields()).from(jb).where(jb.ID.eq(2)).fetchOneInto(TJob.class);
        System.out.println(new Gson().toJson(result));
    }

    /**
     * 查询全部列表
     */
    @Test
	public void selectAll() {
        List<TJobDto> result = dsl.select().from(jb).fetch().map(record->{
        	TJobDto dto = record.into(TJobDto.class);
        	return dto;
        });
        result.forEach(r->{
        	System.out.println(r.getBeanClass());
        });
	}
    
    /**
     * 分页查询全部列表
     */
	@Test
	public void pageSelectAll() {
		int pageNum = 1;
		int pageSize = 10;
        List<TJob> result = dsl.select().from(jb).offset((pageNum-1)*pageSize).limit(pageSize).fetchInto(TJob.class);
        Iterator<TJob> iterator = result.iterator();
        while(iterator.hasNext()) {
        	TJob job = iterator.next();
        	System.out.println(new Gson().toJson(job));
		}
	}
	
}
