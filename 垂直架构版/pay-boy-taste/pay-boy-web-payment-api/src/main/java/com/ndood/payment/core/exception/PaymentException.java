package com.ndood.payment.core.exception;

import com.ndood.payment.core.constaints.PaymentCode;

public class PaymentException extends Exception {
	private static final long serialVersionUID = 13168926815908268L;

	private PaymentCode code;
	
	private String message;

	public PaymentCode getCode() {
		return code;
	}

	public void setCode(PaymentCode code) {
		this.code = code;
	}

	public PaymentException(String message) {
		super(message);
	}

	public PaymentException(PaymentCode code) {
        super(code.getValue());
        this.code = code;
    }
	
	public PaymentException(PaymentCode code, String message) {
		super(message);
        this.code = code;
        this.message = message;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}