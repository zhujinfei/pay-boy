package com.ndood.payment.core.web.aspect;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

@Component
@Aspect
@Slf4j
public class LogAspect {
	/**
	 * 声明一个切入点
	 */
	@Around("execution(* com.ndood.payment.controller.*.*Controller.*(..)) ")
    public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {
		
		String signature = pjp.getSignature().toShortString();
		Stopwatch stopwatch = Stopwatch.createStarted();
		
		StringBuilder pre = new StringBuilder("\n");
		pre.append("111111111111").append(signature).append("\n");
        pre.append(">>>>>>参数：").append(Lists.newArrayList(pjp.getArgs()).toString()).append("\n");
        pre.append("111111111111打印请求信息结束").append(signature).append("\n\n");
        log.info(pre.toString());
        
        Object result = (Object) pjp.proceed(pjp.getArgs());
        StringBuilder sb = new StringBuilder("\n");
        sb.append("222222222222222").append(signature).append("\n");
        sb.append(">>>>>>参数：").append(Lists.newArrayList(pjp.getArgs()).toString()).append("\n");
        String rsJson = JSON.toJSONString(result);
        if(rsJson.length() <= 10000) {
        	sb.append(">>>>>>返回值：").append(rsJson).append("\n");
        }
        sb.append("耗时：").append(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS)).append("(毫秒).").append("\n");
        sb.append("222222222222222打印请求信息结束").append(signature).append("\n\n");
        log.info(sb.toString());
        
        return result;
    
	}
}
