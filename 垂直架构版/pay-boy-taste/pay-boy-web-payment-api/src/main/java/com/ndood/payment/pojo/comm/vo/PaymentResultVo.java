package com.ndood.payment.pojo.comm.vo;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.ndood.payment.core.constaints.PaymentCode;

/**
 * 通用DTO对象
 */
public class PaymentResultVo implements Serializable{
	private static final long serialVersionUID = -4774146968082169416L;

	/**
	 * 请求成功
	 */
	public final static PaymentResultVo ok() {
		return new PaymentResultVo(PaymentCode.SUCCESS);
	}
	
	public final static PaymentResultVo error() {
		return new PaymentResultVo(PaymentCode.ERR_SERVER);
	}
	
	private PaymentCode code;
	private String msg;
	private String error;
	private String next;
	private Object data;
	
	private PaymentResultVo(PaymentCode code) {
		this.code = code;
		this.msg = code.getValue();
	}

	@JSONField(name = "code")
	public int getCode() {
		return code.getCode();
	}
	
	@JSONField(name = "code")
	public PaymentResultVo setCode(int code) {
		this.code = PaymentCode.getEnum(code);
		return this;
	}

	public String getMsg() {
		return msg;
	}
	
	public PaymentResultVo setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public Object getData() {
		return data;
	}

	public PaymentResultVo setData(Object data) {
		this.data = data;
		return this;
	}

	public String getError() {
		return error;
	}

	public PaymentResultVo setError(String error) {
		this.error = error;
		return this;
	}
	
	public String getNext() {
		return next;
	}
	
	public PaymentResultVo setNext(String next) {
		this.next = next;
		return this;
	}

	public String toContent() {
		final StringBuilder sb = new StringBuilder("PaymentResult{");
        sb.append("code='").append(code.getCode()).append('\'');
        sb.append(", msg='").append(msg).append('\'');
        sb.append(", error='").append(error).append('\'');
        sb.append(", data=").append(JSON.toJSON(data));
        sb.append(", next='").append(next).append('\'');
        sb.append('}');
        return sb.toString();
	}
	
}
