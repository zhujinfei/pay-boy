package com.ndood.payment.core.properties;

/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties(prefix = "ndood.payment")
@Data
public class PaymentProperties {
	
	/**
	 * 是否开发模式
	 */
	private Boolean isDevelop = false;
	/**
	 * 支付相关配置
	 */
	private PayConfigProperties pay = new PayConfigProperties();
	/**
	 * oss相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
}