package com.ndood.payment.dao.pay;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ndood.payment.pojo.pay.UserPo;

import io.lettuce.core.dynamic.annotation.Param;

/**
 * User 表数据库控制层接口
 */
public interface UserDao extends BaseMapper<UserPo> {

    /**
     * 自定义注入方法
     */
    int deleteAll();

    @Select("select id as id, nick_name from t_user")
    List<UserPo> selectListBySQL();

    List<UserPo> selectListByWrapper(@Param("ew") Wrapper<?> wrapper);

}