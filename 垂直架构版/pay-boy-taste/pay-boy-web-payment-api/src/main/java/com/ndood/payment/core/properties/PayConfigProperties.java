package com.ndood.payment.core.properties;

import lombok.Data;

/**
 * 微信支付相关配置
 */
@Data
public class PayConfigProperties {
	
	/**
	 * 回调地址
	 */
	private String wechatAppId = "";
	
	private String wechatSubMchId = "";
	
	private String wechatMchId = "";
	
	private String wechatSubAppId = "";
	
	private String wechatKey = "";
	
	private String wechatDomain = "http://www.jeepupil.top";
	
	private String notifyUrl = "http://www.jeepupil.top/demo/wx_pay";

}