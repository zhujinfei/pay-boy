package com.ndood.payment.core.properties;

import lombok.Data;

/**
 * 阿里云OSS配置类
 */
@Data
public class AliyunOssProperties {

	/**
	 * 阿里云外网名
	 */
	private String endPoint = "oss-cn-shanghai.aliyuncs.com";
	/**
	 * 阿里云API的密钥Access Key ID
	 */
	private String accessKeyId = "LTAIu2Dt7B3M4x7R";
	/**
	 * 阿里云API的密钥Access Key Secret
	 */
	private String accessKeySecret = "TAtvJIA0Xd47ZjcChw1Wjg900BfrYS";
	/**
	 * BUCKET
	 */
	private String bucketName = "ndood";
	/**
	 * DIR
	 */
	private String dir = "test/";
	/**
	 * SHARE_DIR
	 */
	private String shareDir = "share/";
	/**
	 * image域名
	 */
	private String imageDomain = "https://imgsr.7xinfo.com";
}
