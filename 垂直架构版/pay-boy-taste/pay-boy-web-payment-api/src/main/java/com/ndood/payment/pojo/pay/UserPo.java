package com.ndood.payment.pojo.pay;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.CreatedDate;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@TableName("t_user")
public class UserPo implements Serializable{

	private static final long serialVersionUID = 14884173055691L;
	
	@TableId
	private Integer id;
	
	@TableField("nick_name")
	private String nickName;
	
	@CreatedDate
	@TableField("create_time")
	private Date createTime;
	
	@TableField("update_time")
	private Date updateTime;
	
}
