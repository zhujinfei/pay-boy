package com.ndood.payment.pojo.comm.vo;

import java.io.Serializable;

public class PaymentErrorVo  implements Serializable{
	private static final long serialVersionUID = -7285807415842181061L;

	private Integer code;
	private String msg;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public PaymentErrorVo(Integer code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public PaymentErrorVo() {
		super();
	}
	
}