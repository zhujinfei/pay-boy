package com.ndood.payment.core.mybatis_plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

/**
 * https://blog.csdn.net/qq_33842795/article/details/80227382
 * https://gitee.com/baomidou/mybatisplus-spring-boot
 * @author ndood
 */
@Configuration
@MapperScan("com.ndood.payment.dao.*")
public class MybatisPlusConfig {

    /**
     * mybatis-plus分页插件<br>
     * 文档：http://mp.baomidou.com<br>
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
