package com.ndood.payment.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.ndood.payment.dao.pay.UserDao;
import com.ndood.payment.pojo.pay.UserPo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDao {
	
	@Autowired
	private UserDao userDao;
	
	@Test
	public void select() {
		List<UserPo> list = userDao.selectListBySQL();
		System.out.println(new Gson().toJson(list));
	}
	
	@Test
	public void selectPage() {
		UserPo po = new UserPo();
		po.setNickName("aaa");
		IPage<UserPo> page = userDao.selectPage(new Page<UserPo>(0, 10), new QueryWrapper<UserPo>().setEntity(po));
		System.out.println(new Gson().toJson(page));
	}
	
	@Test
	public void selectById() {
		UserPo po = userDao.selectById(83);
		System.out.println(new Gson().toJson(po));
	}
	
	@Test
	public void insert() {
		UserPo po = new UserPo();
		po.setNickName("222");
		userDao.insert(po);
	}
	
	@Test
	public void update() {
		UserPo po = new UserPo();
		po.setNickName("");
		po.setId(84);
		userDao.updateById(po);
	}
	
	@Test
	public void delete() {
		userDao.deleteById(84);
	}
	
}
