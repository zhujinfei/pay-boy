package com.ndood.core.social.mini.connect;

import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

import com.ndood.core.social.mini.api.MiniProgram;

/**
 * 微信 api适配器，将微信 api的数据模型转为spring social的标准模型。
 */
public class MiniApiAdapter implements ApiAdapter<MiniProgram> {
	
	public MiniApiAdapter() {}
	
	@Override
	public boolean test(MiniProgram api) {
		return true;
	}

	@Override
	public void setConnectionValues(MiniProgram api, ConnectionValues values) { 
	}

	@Override
	public UserProfile fetchUserProfile(MiniProgram api) {
		return null;
	}

	@Override
	public void updateStatus(MiniProgram api, String message) {
		//do nothing
	}

}