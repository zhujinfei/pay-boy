package com.ndood.core.validate.code;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义验证码异常
 * @author ndood
 */
public class ValidateCodeException extends AuthenticationException{
	
	private static final long serialVersionUID = 6483491120346462280L;

	public ValidateCodeException(String msg) {
		super(msg);
	}
	
}