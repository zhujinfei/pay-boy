package com.ndood.core.properties;

public class MiniProgramProperties {
	
	private String provider_id = "mini"; // 服务商标识
	
	private String app_id = ""; 
	
	private String app_secret = "";

	public String getProvider_id() {
		return provider_id;
	}

	public void setProvider_id(String provider_id) {
		this.provider_id = provider_id;
	}

	public String getApp_id() {
		return app_id;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public String getApp_secret() {
		return app_secret;
	}

	public void setApp_secret(String app_secret) {
		this.app_secret = app_secret;
	}
	
}
