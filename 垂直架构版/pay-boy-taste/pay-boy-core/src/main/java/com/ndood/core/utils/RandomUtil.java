package com.ndood.core.utils;

import java.util.Random;

/**
 * 随机字符串工具类
 * @author ndood
 */
public class RandomUtil {
	
	public static String getRandomString(int length) { // length表示生成字符串的长度
		String base = "abcdefghijklmnopqrstuvwxyz0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(base.length());
			if(random.nextBoolean()){
				sb.append(base.charAt(number));
			}else{
				String c = base.charAt(number)+"";
				sb.append(c.toUpperCase());
			}
		}
		return sb.toString();
	}
	
}
