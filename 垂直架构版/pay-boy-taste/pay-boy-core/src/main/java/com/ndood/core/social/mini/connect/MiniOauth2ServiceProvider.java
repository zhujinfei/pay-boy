package com.ndood.core.social.mini.connect;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

import com.ndood.core.social.mini.api.MiniProgram;
import com.ndood.core.social.mini.api.MiniProgramImpl;

/**
 * 微信的OAuth2流程处理器的提供器，供spring social的connect体系调用
 */
public class MiniOauth2ServiceProvider extends AbstractOAuth2ServiceProvider<MiniProgram> {
	
	/**
	 * 微信获取授权码的url
	 */
	@Deprecated
	private static final String URL_AUTHORIZE = "https://open.weixin.qq.com/connect/qrconnect";
	
	/**
	 * 微信获取accessToken的url
	 */
	private static final String URL_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/jscode2session";

	/**
	 * @param appId
	 * @param appSecret
	 */
	public MiniOauth2ServiceProvider(String appId, String appSecret) {
		super(new MiniOAuth2Template(appId, appSecret,URL_AUTHORIZE,URL_ACCESS_TOKEN));
	}

	@Override
	public MiniProgram getApi(String accessToken) {
		return new MiniProgramImpl(accessToken);
	}

}