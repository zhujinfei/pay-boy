$(function() {
	
	// 获取每个标签的长度
    function f(l) {
        var k = 0;
        $(l).each(function() {
            k += $(this).outerWidth(true)
        });
        return k
    }
    
    // 如果标签页转到了下一页，高亮标签在前一页，重新定位到前一页高亮标签
    function g(n) {
        var o = f($(n).prevAll()),
        q = f($(n).nextAll());
        var l = f($(".content-tabs").children().not(".J_menuTabs"));
        var k = $(".content-tabs").outerWidth(true) - l;
        var p = 0;
        if ($(".page-tabs-content").outerWidth() < k) {
            p = 0
        } else {
            if (q <= (k - $(n).outerWidth(true) - $(n).next().outerWidth(true))) {
                if ((k - $(n).next().outerWidth(true)) > q) {
                    p = o;
                    var m = n;
                    while ((p - $(m).outerWidth()) > ($(".page-tabs-content").outerWidth() - k)) {
                        p -= $(m).prev().outerWidth();
                        m = $(m).prev()
                    }
                }
            } else {
                if (o > (k - $(n).outerWidth(true) - $(n).prev().outerWidth(true))) {
                    p = o - $(n).prev().outerWidth(true)
                }
            }
        }
        $(".page-tabs-content").animate({
            marginLeft: 0 - p + "px"
        },
        "fast")
    }
    
    // 点击左箭头，标签页滑动到前一页
    function a() {
        var o = Math.abs(parseInt($(".page-tabs-content").css("margin-left")));
        var l = f($(".content-tabs").children().not(".J_menuTabs"));
        var k = $(".content-tabs").outerWidth(true) - l;
        var p = 0;
        if ($(".page-tabs-content").width() < k) {
            return false
        } else {
            var m = $(".J_menuTab:first");
            var n = 0;
            while ((n + $(m).outerWidth(true)) <= o) {
                n += $(m).outerWidth(true);
                m = $(m).next()
            }
            n = 0;
            if (f($(m).prevAll()) > k) {
                while ((n + $(m).outerWidth(true)) < (k) && m.length > 0) {
                    n += $(m).outerWidth(true);
                    m = $(m).prev()
                }
                p = f($(m).prevAll())
            }
        }
        $(".page-tabs-content").animate({
            marginLeft: 0 - p + "px"
        },
        "fast")
    }
    
    // 点击右箭头，标签页滑动到后一页
    function b() {
        var o = Math.abs(parseInt($(".page-tabs-content").css("margin-left")));
        var l = f($(".content-tabs").children().not(".J_menuTabs"));
        var k = $(".content-tabs").outerWidth(true) - l;
        var p = 0;
        if ($(".page-tabs-content").width() < k) {
            return false
        } else {
            var m = $(".J_menuTab:first");
            var n = 0;
            while ((n + $(m).outerWidth(true)) <= o) {
                n += $(m).outerWidth(true);
                m = $(m).next()
            }
            n = 0;
            while ((n + $(m).outerWidth(true)) < (k) && m.length > 0) {
                n += $(m).outerWidth(true);
                m = $(m).next()
            }
            p = f($(m).prevAll());
            if (p > 0) {
                $(".page-tabs-content").animate({
                    marginLeft: 0 - p + "px"
                },
                "fast")
            }
        }
    }
    
    // 设置默认的data-index属性
    $(".J_menuItem").each(function(k) {
        if (!$(this).attr("data-index")) {
            $(this).attr("data-index", k)
        }
    });

    // 点击某目录，设置新tab标题
    function c() {
        var o = $(this).attr("href"),
        m = $(this).data("index"),
        l = $.trim($(this).text()),
        k = true;
        if (o == undefined || $.trim(o).length == 0) {
            return true;
        }
        // 如果标签已经存在，则高亮标签，显示该iframe
        $(".J_menuTab").each(function() {
            if ($(this).data("id") == o) {
                if (!$(this).hasClass("active")) {
                    $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
                    g(this);
                }
                k = false;
                return true;
            }
        });
        // 如果标签不存在，则添加高亮新标签，新iframe
        if (k) {
            var p = '<a href="javascript:;" class="active J_menuTab" data-id="' + o + '">' + l + ' <i class="fa fa-times-circle"></i></a>';
            $(".J_menuTab").removeClass("active");
            $(".J_menuTabs .page-tabs-content").append(p);
            g($(".J_menuTab.active"));
        }
        return true;
    }
    $(".J_menuItem").on("click", c);
    
    // TODO 改造 点击每个标签页上的x，关闭当前标签页
    function h() {
        var m = $(this).parents(".J_menuTab").data("id");
        var l = $(this).parents(".J_menuTab").width();
        // 如果关闭的标签是高亮
        if ($(this).parents(".J_menuTab").hasClass("active")) {
        	// 如果关闭的标签后面还有标签，则后面的那个变高亮（关闭中间的会触发，触发后$(this)已失效）
            if ($(this).parents(".J_menuTab").next(".J_menuTab").size()) {
                var k = $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").data("id");
                $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").addClass("active");
                $(".page-content .page-content-body").each(function() {
                    if ($(this).data("id") == k) {
                        $(this).show().siblings(".page-content-body").hide();
                        return false
                    }
                });
                var n = parseInt($(".page-tabs-content").css("margin-left"));
                if (n < 0) {
                    $(".page-tabs-content").animate({
                        marginLeft: (n + l) + "px"
                    },
                    "fast")
                }
                $(this).parents(".J_menuTab").remove();
                $(".page-content .page-content-body").each(function() {
                    if ($(this).data("id") == m) {
                        $(this).remove();
                        return false
                    }
                })
            }
            // 如果关闭的标签前面还有标签(关闭最后一个会触发)
            if ($(this).parents(".J_menuTab").prev(".J_menuTab").size()) {
                var k = $(this).parents(".J_menuTab").prev(".J_menuTab:last").data("id");
                $(this).parents(".J_menuTab").prev(".J_menuTab:last").addClass("active");
                $(".page-content .page-content-body").each(function() {
                    if ($(this).data("id") == k) {
                        $(this).show().siblings(".page-content-body").hide();
                        return false
                    }
                });
                $(this).parents(".J_menuTab").remove();
                $(".page-content .page-content-body").each(function() {
                    if ($(this).data("id") == m) {
                        $(this).remove();
                        return false
                    }
                })
            }
        // 如果关闭的标签非高亮
        } else {
            $(this).parents(".J_menuTab").remove();
            $(".page-content .page-content-body").each(function() {
                if ($(this).data("id") == m) {
                    $(this).remove();
                    return false
                }
            });
            g($(".J_menuTab.active"))
        }
        return false
    }
    $(".J_menuTabs").on("click", ".J_menuTab i", h);
    
    // 点击关闭其它标签，关闭其它标签，以及对应的iframe
    function i() {
        $(".page-tabs-content").children("[data-id]").not(":first").not(".active").each(function() {
            $('.page-content-body[data-id="' + $(this).data("id") + '"]').remove();
            $(this).remove()
        });
        $(".page-tabs-content").css("margin-left", "0")
    }
    $(".J_tabCloseOther").on("click", i);
    
    // 定位被激活的选项卡
    function j() {
        g($(".J_menuTab.active"))
    }
    $(".J_tabShowActive").on("click", j);
    
    // 点击非高亮显示的标签，进行高亮显示
    function e() {
        if (!$(this).hasClass("active")) {
            var k = $(this).data("id"); 
        	var found = false;
        	// 找到对应的page-content-body显示，并隐藏兄弟
            $(".page-content .page-content-body").each(function() {
        		if ($(this).data("id") == k) {
        			$(this).show().siblings(".page-content-body").hide();
        			found = true;
        			return false
        		}
        	});
            // 如果没找到就定位到首页
            if(!found){
            	$(".page-content .page-content-body:first").show().siblings(".page-content-body").hide();
            }
            // 进行高亮显示
            $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
            g(this)
        }
    }
    $(".J_menuTabs").on("click", ".J_menuTab", e);
    
    // 双击标签（没意义）
    function d() {
        var l = $('.page-content-body[data-id="' + $(this).data("id") + '"]');
        var k = l.attr("src")
    }
    $(".J_menuTabs").on("dblclick", ".J_menuTab", d);
    $(".J_tabLeft").on("click", a);
    $(".J_tabRight").on("click", b);
    
    // 改造 点击关闭所有标签选项，关闭所有标签
    $(".J_tabCloseAll").on("click", function() {
        $(".page-tabs-content").children("[data-id]").not(":first").each(function() {
            $('.page-content-body[data-id="' + $(this).data("id") + '"]').remove();
            $(this).remove()
        });
        $(".page-tabs-content").children("[data-id]:first").each(function() {
            $('.page-content-body[data-id="' + $(this).data("id") + '"]').show();
            $(this).addClass("active")
        });
        $(".page-tabs-content").css("margin-left", "0")
    });
    
    $(".J_tabRefresh").on("click", function() {
    	// 获取高亮tab的data-id作为url
    	$(".page-tabs-content").children("[data-id]").not(":first").each(function() {
    		if($(this).hasClass("active")){
    			var url = $(".J_menuTab.active").data("id");
    			Layout.reloadAjaxContent(url);
    		}
    	});
    })
});