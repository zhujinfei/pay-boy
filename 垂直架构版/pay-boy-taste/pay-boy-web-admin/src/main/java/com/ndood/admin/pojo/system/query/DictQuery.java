package com.ndood.admin.pojo.system.query;

import java.util.Date;

import com.ndood.admin.pojo.system.DictPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 字典查询类
 * @author ndood
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class DictQuery extends DictPo {
	private static final long serialVersionUID = -5310767646766821702L;
	private Integer offset;
	private Integer limit;
	private String keywords;
	private Date startTime;
	private Date endTime;

	public int getPageNo() {
		return offset / limit;
	}

}
