package com.ndood.admin.pojo.comm.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 文件上传结果返回DTO
 */
@Getter @Setter
public class FileUploadDto implements Serializable{
	private static final long serialVersionUID = -6598918597879408675L;
	
	/**
	 * 图片url
	 */
	private String url;
	/**
	 * 图片名称
	 */
	private String name;
	/**
	 * 图片上传日期
	 */
	private Date uploadDate;
	/**
	 * 图片大小
	 */
	private long size;
	
}
