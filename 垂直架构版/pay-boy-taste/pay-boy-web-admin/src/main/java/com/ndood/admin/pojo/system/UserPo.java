package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.security.SocialUserDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.api.client.util.Maps;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户bean
 * @author ndood
 */
@Entity
@Table(name="t_user")
@Cacheable(true)
@Getter @Setter
//@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})
@JsonIgnoreProperties(value={"handler","fieldHandler"})
public class UserPo implements UserDetails, SocialUserDetails, Serializable{
	private static final long serialVersionUID = 4009471135416449773L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 密码
	 */
	@Column(length = 100, nullable = false)
	private String password;
	
	/**
	 * 昵称
	 */
	@Column(name="nick_name", length = 255)
	private String nickName;
	
	/**
	 * 性别
	 */
	private Integer sex;
	
	/**
	 * 出生日期
	 */
	private String birthday;
	
	/**
	 * 头像
	 */
	@Column(name="head_img_url", length = 255)
	private String headImgUrl;
	
	/**
	 * 邮箱
	 */
	@Column(length = 32)
	private String email;
	
	/**
	 * 邮箱激活状态
	 */
	@Column(name="email_status")
	private Integer emailStatus;
	
	/**
	 * 邮箱随机字串
	 */
	@Column(name="email_nonce_str",length = 100)
	private String emailNonceStr;
	
	/**
	 * 手机
	 */
	@Column(length = 32, nullable = false, unique = true)
	private String mobile;
	
	/**
	 * 状态 0：未启用 1：启用
	 */
	@Column(nullable = false)
	private Integer status;
	
	/**
	 * 排序字段
	 */
	@Column(nullable = false)
	private Integer sort;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time", nullable = false, columnDefinition="datetime", updatable=false)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time", nullable = false, columnDefinition="datetime")
	private Date updateTime;
	
	/**
	 * 地址
	 */
	@Column(length = 50)
	private String address;
	
	/**
	 * 地址
	 */
	@Column(name="social_user_id", length = 80)
	private String socialUserId;
	
	/**
	 * 省
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "province_id", referencedColumnName = "id")
	private RegionPo province;
	
	/**
	 * 市
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city_id", referencedColumnName = "id")
	private RegionPo city;
	
	/**
	 * 区
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "district_id", referencedColumnName = "id")
	private RegionPo district;
	
	/**
	 * 部门
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dept_id", referencedColumnName = "id")
	private DepartmentPo department;
	
	/**
	 * 角色
	 */
	@ManyToMany
    @JoinTable(name = "t_user_role", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<RolePo> roles;
	
	/**
	 * 用户有权访问的所有url，不持久化到数据库
	 */
	/*private Set<String> urls = new HashSet<>();*/
	@Transient
	private Map<String,GrantedAuthority> urlMap = Maps.newHashMap();
	
	/**
	 * 将urls的东西全都放到authorities中，看看角色是否都要加ROLE_
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		/*Set<GrantedAuthority> set = new HashSet<GrantedAuthority>();
		if(urls == null) {
			return set;
		}
		for (String url : urls) {
			if(StringUtils.isEmpty(url)) {
				continue;
			}
			set.add(new SimpleGrantedAuthority(url));
		}
		return set;*/
		return urlMap.values();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getUserId() {
		return String.valueOf(id);
	}

	@Override
	public String getUsername() {
		/*if(this.getMobile()!=null) {
			return this.getMobile();
		}else if(this.getEmail()!=null) {
			return this.getEmail();
		}
		return null;*/
		// security登录内存
		return String.valueOf(id);
	}
}
