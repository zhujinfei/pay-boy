package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * 资源bean
 * @author ndood
 */
@Entity
@Table(name="t_permission")
@Cacheable(true)
@Getter @Setter
@JsonIgnoreProperties(value={"handler","fieldHandler"})
public class PermissionPo implements Serializable{
	private static final long serialVersionUID = 7048865939808880449L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 资源名称（权限）
	 */
	@Column(length = 32, nullable = false, unique = true)
    private String name;
	
	/**
	 * 资源描述
	 */
	@Column(name="`desc`", length = 32)
    private String desc;
    
    /**
     * 资源图标
     */
	@Column(length = 32)
    private String icon;
	
	/**
     * 资源排序
     */
	@Column(nullable = false)
    private Integer sort;
	
	/**
     * 资源类型
     */
	@Column(nullable = false)
    private Integer type;
	
	/**
	 * 资源URL
	 */
    @Column(length=255)
    private String url;
	
    /**
	 * 删除标记
	 */
	@Column(name="status", nullable = false)
	private Integer status;
    
	/**
	 * 创建时间
	 */
	@Column(name="create_time", nullable = false, columnDefinition="datetime", updatable=false)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time", nullable = false, columnDefinition="datetime")
	private Date updateTime;
    
    /**
     * 上级资源
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="parent_id", referencedColumnName = "id")
    private PermissionPo parent;
    
    /**
     * 下级资源
     */
    @OneToMany(cascade=CascadeType.REMOVE,fetch=FetchType.LAZY)
    @JoinColumn(name="parent_id", referencedColumnName = "id")
    private List<PermissionPo> children;
    
}
