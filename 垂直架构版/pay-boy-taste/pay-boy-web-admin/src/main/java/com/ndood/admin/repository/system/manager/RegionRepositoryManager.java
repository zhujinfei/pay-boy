package com.ndood.admin.repository.system.manager;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RegionRepositoryManager {
	
	@Autowired
	@PersistenceContext
	private EntityManager em;

	/**
	 * 根据父节点ID查询出子节点数量
	 */
	public Long countChildrenByParentId(Integer parentId) {

		// Step2: 获取list
		Query query = em.createQuery("SELECT COUNT(0) "
				+ "FROM RegionPo r "
				+ "WHERE 1 = 1 AND r.parent.id = :parentId ");
		
		query.setParameter("parentId", parentId);
		
		query.setHint("org.hibernate.cacheable", true);
		Long count = (Long) query.getSingleResult();
		
		return count;
	}
	
}
