package com.ndood.admin.pojo.system.bo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CityBo implements Serializable {
	private static final long serialVersionUID = 8131498471676180807L;

	String country;

    String province;

    String city;
}