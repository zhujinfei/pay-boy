package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * 部门bean
 * https://blog.csdn.net/yiduyangyi/article/details/54176453
 * https://blog.csdn.net/yhflyl/article/details/81511380
 * @author ndood
 */
@Entity
@Table(name="t_department")
@Getter @Setter
@Cacheable(true)
@JsonIgnoreProperties(value={"handler","fieldHandler"})
public class DepartmentPo implements Serializable{
	private static final long serialVersionUID = 1637316527580276065L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 名称
	 */
	@Column(length = 32, nullable = false, unique = true)
	private String name;

	/**
	 * 排序
	 */
	private Integer sort;
	
	/**
	 * 状态：0 禁用 1 启用
	 */
	private Integer status;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time", nullable = false, columnDefinition="datetime", updatable=false)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time", nullable = false, columnDefinition="datetime")
	private Date updateTime;

	/**
	 * 父部门
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	private DepartmentPo parent;
	
	/**
	 * 子部门
	 */
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	private List<DepartmentPo> children;
	
	/**
	 * 部门拥有的用户
	 */
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	@JoinColumn(name = "dept_id", referencedColumnName = "id")
	private List<UserPo> users;

}
