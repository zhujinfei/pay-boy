package com.ndood.admin.core.properties;

import lombok.Data;

/**
 * 阿里云OSS配置类
 */
@Data
public class AliyunOssProperties {

	/**
	 * 阿里云外网名
	 */
	private String endPoint;
	/**
	 * 阿里云API的密钥Access Key ID
	 */
	private String accessKeyId;
	/**
	 * 阿里云API的密钥Access Key Secret
	 */
	private String accessKeySecret;
	/**
	 * BUCTKET
	 */
	private String bucketName="ndood";
	/**
	 * Folder Dir
	 */
	private String dir="test/";
	/**
	 * 图片域名
	 */
	private String domain = "";
}
