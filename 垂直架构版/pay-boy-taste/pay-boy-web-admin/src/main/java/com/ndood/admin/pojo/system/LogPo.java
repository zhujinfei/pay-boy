package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="t_log")
@Cacheable(true)
@Getter @Setter
@JsonIgnoreProperties(value={"handler","fieldHandler"})
public class LogPo implements Serializable{
	private static final long serialVersionUID = -9095624906062083438L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	/**
	 * ip
	 */
	private String ip;
	/**
	 * ip信息
	 */
	@Column(name="`ip_info`")
	private String ipInfo;
	/**
	 * 请求类型
	 */
	@Column(name="`request_type`")
	private String requestType;
	/**
	 * 请求地址
	 */
	@Column(name="`request_url`")
	private String requestUrl;
	/**
	 * 请求参数
	 */
	@Column(name="`request_param`")
	private String requestParam;
	/**
	 * 账户名
	 */
	private String username;
	/**
	 * 创建时间
	 */
	@Column(name="`create_time`")
	private Date createTime;
	
}
