package com.ndood.admin.pojo.system.bo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Exrickx
 */
@Getter @Setter
public class IpLocateBo implements Serializable {
	private static final long serialVersionUID = 8902007031223245460L;

	private String retCode;

    private CityBo result;
}