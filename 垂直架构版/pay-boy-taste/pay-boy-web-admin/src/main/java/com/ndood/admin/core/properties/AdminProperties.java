package com.ndood.admin.core.properties;
/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * ndood admin配置主类
 * @author ndood
 */
@Data
@ConfigurationProperties(prefix = "ndood.admin")
public class AdminProperties {
	
	private String domain = "http://www.payboy.top";
	
	/**
	 * 阿里云通信相关配置
	 */
	private AliyunSmsProperties dayu = new AliyunSmsProperties();
	/**
	 * 阿里云OSS相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
	/**
	 * 静态资源相关配置
	 */
	private StaticResourceProperties stat = new StaticResourceProperties();
	/**
	 * 邮件相关配置
	 */
	private AdminEmailProperties email = new AdminEmailProperties();
}