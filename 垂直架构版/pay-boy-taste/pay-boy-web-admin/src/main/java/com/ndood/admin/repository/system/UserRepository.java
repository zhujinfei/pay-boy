package com.ndood.admin.repository.system;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.UserPo;

/**
 * 用户dao
 * @author ndood
 */
public interface UserRepository extends JpaRepository<UserPo, Integer>{
	
	Page<UserPo> findAll(Specification<UserPo> specification, Pageable pageable);

	//UserPo findByUsername(String username);

	UserPo findByMobile(String username);
	
	UserPo findBySocialUserId(String socialUserId);

	//Long countByUsername(String username);

	Long countByMobile(String mobile);

	Long countByEmail(String email);

	UserPo findByEmail(String email);

	UserPo findByEmailAndEmailStatus(String email, Integer emailStatus);

}
