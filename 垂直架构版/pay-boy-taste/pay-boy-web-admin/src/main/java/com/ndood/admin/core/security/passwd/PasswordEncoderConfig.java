package com.ndood.admin.core.security.passwd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 密码配置类
 */
@Configuration
public class PasswordEncoderConfig {
	
	@Bean
	public PasswordEncoder customPasswordEncoder() {

		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence rawPassword) {
				return BCrypt.hashpw(rawPassword.toString(), BCrypt.gensalt(4));
			}

			@Override
			public boolean matches(CharSequence encodedPassword, String rawPassword) {
				return BCrypt.checkpw(rawPassword, encodedPassword.toString());
			}
		};
		
	}
	
}
